public class Ausgabenformatierung {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String sTemp_Typ1 = "Celsius";
		String sTemp_Typ2 = "Fahrenheit";
		String minus = "-";
		short fTemp_1 = -20;
		float cTemp_1 = -28.8889f;
		short fTemp_2 = -10;
		float cTemp_2 = -23.3333f;
		short fTemp_3 = 0;
		float cTemp_3 = -17.7778f;
		short fTemp_4 = 20;
		float cTemp_4 = -6.6667f;
		short fTemp_5 = 30;
		float cTemp_5 = -1.1111f;

		// Kopfzeile schreiben
		System.out.printf("%-13s|%10s\n", sTemp_Typ2, sTemp_Typ1);
		// Minus Setzen
		for (int i = 0; i < 24; i++) {
			System.out.print(minus);
		}
		// Ausgabe Temperatur Werte
		System.out.printf("\n%-13s|%10.2s", fTemp_1, cTemp_1);
		System.out.printf("\n%-13s|%10.2s", fTemp_2, cTemp_2);
		System.out.printf("\n%-13s|%10.2s", fTemp_3, cTemp_3);
		System.out.printf("\n%-13s|%10.2s", fTemp_4, cTemp_4);
		System.out.printf("\n%-13s|%10.2s", fTemp_5, cTemp_5);
	}
		
}
