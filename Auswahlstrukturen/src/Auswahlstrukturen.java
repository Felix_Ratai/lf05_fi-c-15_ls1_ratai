import java.util.Scanner;

public class Auswahlstrukturen {

	public static void main(String[] args) {

		Scanner Scanner = new Scanner(System.in);
		System.out.println("Bitte geben sie die Erste Zahl ein ");
		int z1 = Scanner.nextInt();
		System.out.println("Bitte geben sie die Zweite Zahl ein ");
		int z2 = Scanner.nextInt();
		System.out.println("Bitte geben sie die Dritte Zahl ein ");
		int z3 = Scanner.nextInt();
		aufgabe2(z1, z2);
		aufgabe3(z1, z2);
		aufgabe4(z1, z2);
		aufgabe2_1(z1, z2, z3);
		aufgabe2_2(z1, z2, z3);
		aufgabe2_3(z1, z2, z3);
	}

	public static void aufgabe2(int z1, int z2) {
		if (z1 == z2) {
			System.out.println("Die Erste Zahl und die Zweite sind gleich");
		}
	}

	public static void aufgabe3(int z1, int z2) {
		if (z2 > z1) {
			System.out.println("Die Zweite Zahl ist gr��er als die Erste");
		}
	}

	public static void aufgabe4(int z1, int z2) {
		if (z1 >= z2) {
			System.out.println("Die Erste Zahl ist gr��er oder gleich der Zweiten");
		}
	}

	public static void aufgabe2_1(int z1, int z2, int z3) {
		if (z1 > z2 && z1 > z3) {
			System.out.println("Die Erste Zahl ist gr��er als die zweite und dritte");
		}
	}

	public static void aufgabe2_2(int z1, int z2, int z3) {
		if (z3 > z2) {
			System.out.println("Die Dritte Zahl ist gr��er als die Zweite");
		} else if (z3 > z1) {
			System.out.println("Die Dritte Zahl ist gr��er als die Erste");
		}

	}

	public static void aufgabe2_3(int z1, int z2, int z3) {

		if (z1 > z2 && z1 > z3) {
			System.out.println("Die Erste Zahl ist die Gr��te");

		} else if (z2 > z1 && z2 > z3) {
			System.out.println("Die Zweite Zahl ist die Gr��te");

		} else if (z3 > z2 && z3 > z1) {
			System.out.println("Die Dritte Zahl ist die Gr��te");

		}
	}

}
