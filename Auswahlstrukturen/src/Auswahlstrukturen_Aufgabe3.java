import java.util.Scanner;

public class Auswahlstrukturen_Aufgabe3 {

	public static void main(String[] args) {
		Scanner Scanner = new Scanner(System.in);
		System.out.println("Wie Viele Computermäuse wollen sie Bestellen ");
		int mause = Scanner.nextInt();
		System.out.println("Bitte geben sie den Nettopreis ein ");
		double netto = Scanner.nextDouble();
		final double mwstSatz = 1.19;
		double bruttopreis = preisberechnung(netto, mause, mwstSatz);
		ausgabe(bruttopreis);
		Scanner.close();
	}

	public static double preisberechnung(double netto, int mause, double mwstSatz) {
		double bruttopreis = netto;
		double einzelpreis;
		if (mause < 10) {
			einzelpreis = netto * mwstSatz + 10;
			bruttopreis = einzelpreis*mause;}
		else if (mause >= 10) {
			einzelpreis = netto * mwstSatz;
			bruttopreis = einzelpreis*mause;}
		return bruttopreis;
	}

	public static void ausgabe(double bruttopreis) {
		System.out.printf("Der Bruttopreis beträgt %.2f", bruttopreis);
	}
}
