import java.util.Scanner;
public class Auswahlstrukturen_Aufgabe4 {

	public static void main(String[] args) {
		final double mwstSatz = 1.19;
		Scanner Scanner = new Scanner(System.in);
		System.out.println("Bitte geben sie den Bestellwert ein");
		double bestellwert = Scanner.nextDouble();
		double bruttowert = rabattberechnung(mwstSatz, bestellwert);
		System.out.printf("Der Bruttopreis mit Rabatt betr�gt %.2f", bruttowert);
		Scanner.close();
	}
	public static double rabattberechnung (double bestellwert, double mwstSatz) {
		double bruttowert = -1;
		double rabatt;
		if (bestellwert <= 100) {
			rabatt = 0.90;
			bruttowert = bestellwert*rabatt*mwstSatz;
			return bruttowert;
		}
		else if (bestellwert <= 500) {
			rabatt = 0.85;
			bruttowert = bestellwert*rabatt*mwstSatz;
			return bruttowert;
	}
		else {
			rabatt =0.80;
			bruttowert = bestellwert*rabatt*mwstSatz;
			return bruttowert;}
	}
	
}
