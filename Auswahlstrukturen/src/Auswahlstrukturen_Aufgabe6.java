import java.util.Scanner;
import java.lang.Math;

public class Auswahlstrukturen_Aufgabe6 {

	public static void main(String[] args) {
		Scanner Scanner = new Scanner(System.in);
		double ergebnis= -1;
		System.out.println("Die zu Berechnende Funktion ist y=f(x)");
		System.out.println("Bitte geben sie den wert von x ein");
		System.out.println("Anhand des wertes von x wird die Funktion f Festgelegt");
		System.out.println("x ist kleiner oder gleich 0 ist die genutzte Funktion Exponentiell");
		System.out.println("x ist gr��er 0 aber kleiner 3 ist die genutzte Funktion Quadratisch");
		System.out.println("x ist gr��er 3 ist die genutzte Funktion Linear");
		double x = Scanner.nextDouble();
		
		if (x <= 0) {
			ergebnis = exponentiell(x);
			System.out.println("Der Funktionsbereich ist exponentiell");
		} else if (x <= 3) {
			ergebnis = quadratisch(x);
			System.out.println("Der Funktionsbereich ist quadratisch");
		} else if (x > 3) {
			ergebnis = linear(x);
			System.out.println("Der Funktionsbereich ist Linear");
		}
		System.out.println(ergebnis);
	}

	public static double exponentiell(double x) {
		double e = 2.718;
		e = Math.pow(x, e);
		return e;
	}

	public static double quadratisch(double x) {
		x = x * x + 1;
		return x;
	}

	public static double linear(double x) {
		x = x * 2 + 4;
		return x;
	}
}
