import java.util.Scanner;

public class Auswahlstrukturen_Aufgabe7 {

	public static void main(String[] args) {
		Scanner Scanner = new Scanner(System.in);
		System.out.println("M�chten sie Buchstaben oder Zahlen Sortieren? Geben sie B f�r Buchstaben oder Z f�r Zahlen ein danke.");
		char eingabe = Scanner.next().charAt(0);
		if (eingabe == 'B') {
			System.out.println("Bitte geben sie den Ersten Buchstaben ein");
			char erster = Scanner.next().charAt(0);
			System.out.println("Bitte geben sie den Zweiten Buchstaben ein");
			char zweiter = Scanner.next().charAt(0);
			System.out.println("Bitte geben sie den Dritten Buchstaben ein");
			char dritter = Scanner.next().charAt(0);
			sortierung_Buchstabe(erster, zweiter, dritter);
		} else if (eingabe == 'Z') {
			System.out.println("Bitte geben sie die Erste Zahl ein");
			int erster = Scanner.nextInt();
			System.out.println("Bitte geben sie die Zweite Zahl ein");
			int zweiter = Scanner.nextInt();
			System.out.println("Bitte geben sie die Dritte Zahl ein");
			int dritter = Scanner.nextInt();
			sortierung_Zahl(erster, zweiter, dritter);
		}
		else {System.out.println("Bitte geben sie B oder Z ein");}
		Scanner.close();
	}

	public static void sortierung_Zahl(int erster, int zweiter, int dritter) {
		int temp;
		if (erster > zweiter) {
			temp = erster;
			erster = zweiter;
			zweiter = temp;
		}
		if (erster > dritter) {
			temp = erster;
			erster = dritter;
			dritter = temp;
		}
		if (zweiter > dritter) {
			temp = zweiter;
			zweiter = dritter;
			dritter = temp;
		}
		System.out.println(erster + " " + zweiter + " " + dritter);
	}

	public static void sortierung_Buchstabe(char erster, char zweiter, char dritter) {
		char temp;
		if (erster > zweiter) {
			temp = erster;
			erster = zweiter;
			zweiter = temp;
		}
		if (erster > dritter) {
			temp = erster;
			erster = dritter;
			dritter = temp;
		}
		if (zweiter > dritter) {
			temp = zweiter;
			zweiter = dritter;
			dritter = temp;
		}
		System.out.println(erster + " " + zweiter + " " + dritter);
	}
}
