﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] arg) {

		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
		double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(rückgabebetrag);
	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderBetrag;
		int anzahlFahrkarten;

		System.out.print("Zu zahlender Betrag (EURO): ");
		zuZahlenderBetrag = tastatur.nextDouble();
		if (zuZahlenderBetrag < 0) {
			System.out.println("Bitte geben sie einen Positiven wert für den Ticketpreis in Euro an");
			System.out.println("Es wird mit einem Ticketpreis von 1€ fortgefahren");
			zuZahlenderBetrag = 1;
		}
		System.out.print("Wie Viele Tickets möchten sie ");
		anzahlFahrkarten = tastatur.nextInt();
		if (anzahlFahrkarten < 10) {
			System.out.println("Fehler! Die Anzahl der bestellten Tickets beträgt mehr als 10");
			System.out.println("Es wird mit einer Ticketanzahl von 1 Fortgefahren");
			anzahlFahrkarten = 1;
		}
		zuZahlenderBetrag = zuZahlenderBetrag * anzahlFahrkarten;

		return zuZahlenderBetrag;

	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double rückgabebetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		Scanner tastatur = new Scanner(System.in);
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: " + "%.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.println(" Euro");
			// System.out.println("Anzahl der Tickets "+ anzahlFahrkarten);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		tastatur.close();
		return rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;

	}

	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrscheine werden ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

	}

	public static void rueckgeldAusgeben(double rückgabebetrag) {
		// Rückgeldberechnung und -Ausgabe
		// -------------------------------

		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}

}
// Aufgabe 2.5 5 Begründen Sie Ihre Entscheidung für die Wahl des Datentyps.
// Ich habe mich für den datentyp integer entschieden da es sich bei der ticket
// anzahl um eine gerade zahl handelt
// Aufgabe 2.5 6 Erläutern Sie detailliert, was bei der Berechnung des Ausdrucks
// anzahl * einzelpreis passiert.
// in Zeile 22 sehen sie die berechnung dort wird die variable des
// einzelticketpreises mit der variable der anzahl der tickets mal genommen und
// das ergebnis wird als zuZahlenderBetrag gespeichert
// zuZahlenderBetrag steht für den noch zu zahlenden betrag des nutzers dieser
// kriegt einen wert festgelegt wenn der nutzer in der ersten aufforderung
// diesen eintippt
// anzahlFahrkarten steht für die anzahl der Fahrkarten die der nutzer kaufen
// möchte dieser kriegt einen wert festgelegt wenn der nutzer in der zweiten
// aufforgederung diesen eintippt
// der* ist das rechenzeichen für eine multiplikation