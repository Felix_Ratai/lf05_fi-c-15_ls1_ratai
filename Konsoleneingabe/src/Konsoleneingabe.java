import java.util.Scanner; // Import der Klasse Scanner

public class Konsoleneingabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Neues Scanner-Objekt myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);

		// Abfragen und Speichern des Namen
		System.out.print("Guten Tag wie hei�en sie denn?");
		String name = myScanner.next();
		System.out.print("Wie alt sind sie denn?");
		int alter = myScanner.nextInt();

		System.out.print("Hallo " + name + " sie sind " + alter + " jahre alt");

		myScanner.close();

	}

}
