import java.util.Scanner;

public class Aufgabe2_while {

	public static void main(String[] args) {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Bitte geben sie ein bis zu welcher zahl sie die Fakultät haben moechten");
			int ziel = scanner.nextInt();
			int summe = 1;
			int zaehler= 1;
			while (ziel>=zaehler) {
				summe = summe*zaehler;
				zaehler++;
			}
			System.out.println("Die Fakultät ist "+ summe);
	}

}
