public class Aufgabe4_For {
	public static void main(String[] args) {
		System.out.println("Ausgabe der Zahlenfolgen:");

		int zaehler = 1;
		int folge4 = 2;
		int folge5 = 2;
		int folge2temp = 1;
		int folge4temp = 2;

		for (int i = 99; i >= 9; i = i - 3) {
			System.out.print(i);
			if (i > 9)
				System.out.print(",");
			if (i <= 9) {
				System.out.println("");
			}
		}
		for (int i2 = 1; i2 <= 400; i2 = zaehler * zaehler) {
			System.out.print(i2);
			if (zaehler < 20)
				System.out.print(",");
			if (zaehler >= 20) {
				System.out.println("");
			}
			if (i2 <= 1) {
				i2++;
			}
			zaehler++;
		}

		for (int i3 = 2; i3 <= 102; i3 = i3 + 4) {
			System.out.print(i3);
			if (i3 < 102)
				System.out.print(",");
			if (i3 >= 102) {
				System.out.println("");
			}
		}
		for (int i4=2;i4<=32;i4=i4+2) {
			int i4print = (int) Math.pow(i4, folge4temp);
			System.out.print(i4print);
			if (i4 < 32) {
				System.out.print(",");
			}
			if (i4 >= 32) {
				System.out.println("");
			}
		}
		for (int i5=2;i5 <= 32768;i5=i5*2) {
			System.out.print(i5);
			if (i5 <= 16384) {
				System.out.print(",");
			}

		}
	}
}
