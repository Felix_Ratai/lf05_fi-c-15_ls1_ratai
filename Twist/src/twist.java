import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Arrays;

public class twist {
	public static void main(String[] arg) {
		Scanner scan = new Scanner(System.in);
		char auswahl = 0;
		do { // Der do while loop ist für die Nutzereingabe zuständig und gibt einen Fehler
				// aus wenn der Nutzer eine Falsche eingabe macht und fordert ihn zu erneurter
				// eingabe auf
			System.out.println("Bitte geben sie an ob sie ein Wort twisten oder untwisten wollen");
			System.out.println("Ein t für twisten oder ein u für untwisten");
			auswahl = scan.next().charAt(0);

			if (auswahl == 't') {
				System.out.println("Bitte geben sie das Wort ein das sie Twisten wollen");
				System.out.println("Bitte achten sie auf Korrekte Groß und Kleinschreibung");
			} else if (auswahl == 'u') {
				System.out.println("Bitte geben sie das getwistete Wort ein was Untwistet werden soll");
				System.out.println("Bitte achten sie auf Korrekte Groß und Kleinschreibung");
			} else {
				System.out.println("Bitte geben sie entweder t f�r twisten oder u f�r untwisten an");
			}
		} while (auswahl != 't' && auswahl != 'u');
		String eingabe = scan.next(); // Eingabe des wortes
		char[] eingabeC = eingabe.toCharArray(); // Der eingegebene String wird in einen Char array gespeichert dad
													// ieser leichter zu bearbeiten ist
		char erster = eingabeC[0]; // Der erste und der Letzte buchstabe werden gespeichert f�r sp�tere bearbeitung
		char letzter = eingabeC[eingabeC.length - 1];
		String loesung = "Error";
		if (auswahl == 't') {
			loesung = shuffle(erster, letzter, eingabeC);// Methode zum twisten des Wortes
		} else if (auswahl == 'u') {
			loesung = unshuffle(erster, letzter, eingabeC);// Methode zum untwisten eines wortes das der Nutzer eingibt
		}
		ausgabe(loesung); // Ausgabe und Umwandlung des Char arrays zum String
		scan.close(); // Schlie�en des Scanners zum vermeiden eines Resource Leaks
	}

	public static String shuffle(char erster, char letzter, char[] eingabeC) {
		Random r = new Random();
		int ordertemp = 0;
		int maxlaenge = eingabeC.length - 1;
		for (int i = maxlaenge; i > 0; i--) {// Shufflen des wortes nach Fisher-Yates shuffle
			int randomtemp = r.nextInt(i + 1);
			char sorttemp = eingabeC[i];
			eingabeC[i] = eingabeC[randomtemp];
			eingabeC[randomtemp] = sorttemp;
		}
		while (erster != eingabeC[0] || letzter != eingabeC[maxlaenge]) {// Einsetzten des ersten und Letzen zeichen des
																			// arrays an den gewollten positionen
			if (erster == eingabeC[ordertemp]) {
				char sorttemp2 = eingabeC[0];
				eingabeC[0] = eingabeC[ordertemp];
				eingabeC[ordertemp] = sorttemp2;
			}
			if (letzter == eingabeC[ordertemp]) {
				char sorttemp2 = eingabeC[maxlaenge];
				eingabeC[maxlaenge] = eingabeC[ordertemp];
				eingabeC[ordertemp] = sorttemp2;
			}

			if (erster != eingabeC[0] || letzter != eingabeC[maxlaenge]) {
				ordertemp += 1;
			}
		}

		String loesung = String.copyValueOf(eingabeC);
		return (loesung);
	}

	public static String unshuffle(char erster, char letzter, char[] eingabeC) {
		Scanner scanner = null; // Erstellen des Scanner objekts au�erhalb von try/catch da sonst eine
								// möglichkeit besteht das der Scanner nicht erstellt wird
		try {
			scanner = new Scanner(new File(
					"C:\\Users\\admin\\Documents\\Osz Imt\\git\\lf05_fi-c-15_ls1_ratai\\Twist\\bin\\woerterliste.txt"));// Erstellen
																														// des
																														// scanners
			// mit der
			// woerterliste.txt als
			// fokus
		} catch (FileNotFoundException e) { // Ausnahme hinzufügen die einen Fehler ausgibt wenn der dateipfad oben
											// inkorrekt ist
			e.printStackTrace();
		}
		while (scanner.hasNextLine()) { // scanner.hasnextline läuft solange bis in der txt datei keine neue zeile mehr
										// verfügbar ist
			String woerterbuch = scanner.nextLine();// scanner.nextline erfasst eine neue zeile und deren inhalt in
													// unserem textdokument
			char[] woerterbuchC = woerterbuch.toCharArray();// umwandeln zu char array für den vergleich
			char woerterbuchCletzter = woerterbuchC[woerterbuch.length() - 1];
			char woerterbuchCerster = woerterbuchC[0];
			if (erster == woerterbuchCerster && woerterbuchCletzter == letzter) {
				if (woerterbuchC.length == eingabeC.length) {
					Arrays.sort(eingabeC);// Sortieren des eingabe arrays nach wert für den vergleich
					Arrays.sort(woerterbuchC);// sortieren des erfassten wortes nach wer für den vergleich
					if (Arrays.equals(eingabeC, woerterbuchC)) {// Wenn der wert beider worte übereinstimmt dann wird es
																// zurückgegeben
						return woerterbuch;
					}
				}
			}
		}
		return null;// Falls kein wort gefunden werden kann wird null zurückgegeben
	}

	public static void ausgabe(String loesung) {
		System.out.println("Das  wort ist " + loesung);
	}
}